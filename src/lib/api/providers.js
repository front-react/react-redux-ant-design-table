import axios from "axios";

const clientsApiProvider = axios.create({
  baseURL: process.env.REACT_APP_API_CLIENTS_ENDPOINT,
});

const modelesApiProvider = axios.create({
  baseURL: process.env.REACT_APP_API_MODELES_ENDPOINT,
});

const sousModelesApiProvider = axios.create({
  baseURL: process.env.REACT_APP_API_SOUS_MODELES_ENDPOINT,
});

const produitsApiProvider = axios.create({
  baseURL: process.env.REACT_APP_API_PRODUITS_ENDPOINT,
});

const prescripteursApiProvider = axios.create({
  baseURL: process.env.REACT_APP_API_PRESCRIPTEURS_ENDPOINT,
});

const pdcApiUpsellProvider = axios.create({
  baseURL: process.env.REACT_APP_API_PDC_ENDPOINT,
});

const emplacementsApiUpsellProvider = axios.create({
  baseURL: process.env.REACT_APP_API_EMPLACEMENTS_ENDPOINT,
});

const iocApiUpsellProvider = axios.create({
  baseURL: process.env.REACT_APP_API_IOC_ENDPOINT,
});

const kpiApiUpsellProvider = axios.create({
  baseURL: process.env.REACT_APP_API_KPI_ENDPOINT,
});

const enquetesApiUpsellProvider = axios.create({
  baseURL: process.env.REACT_APP_API_ENQUETES_ENDPOINT,
});

export {
  clientsApiProvider,
  modelesApiProvider,
  sousModelesApiProvider,
  produitsApiProvider,
  prescripteursApiProvider,
  pdcApiUpsellProvider,
  emplacementsApiUpsellProvider,
  iocApiUpsellProvider,
  kpiApiUpsellProvider,
  enquetesApiUpsellProvider,
};
