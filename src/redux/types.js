export const ERROR_HANDLER = "portail/ERROR_HANDLER";

export const AUTH_LOGIN = "auth/LOGIN";
export const AUTH_LOGOUT = "auth/AUTH_LOGOUT";
export const AUTH_GET_TOKEN = "auth/AUTH_GET_TOKEN";
export const AUTH_SET_IDENTITY = "auth/AUTH_SET_IDENTITY";
export const SET_IDENTITY_USER = "user/SET_IDENTITY_USER";

export const SIDEBAR_COLLAPSE = "layout/SIDEBAR_COLLAPSE";

//Error for Login
export const ERROR_STATUS_LOGIN = "user/ERROR_STATUS_LOGIN";
export const NOTIFICATION_STATUS_LOGIN = "user/NOTIFICATION_STATUS_LOGIN";
