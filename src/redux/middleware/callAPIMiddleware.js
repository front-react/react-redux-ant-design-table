//import displayError from "../../actions/displayError";

const dispatchAction = (type, data = null, payload = null) => {
  return {
    type,
    payload: {
      ...payload,
      data,
    },
  };
};

export const callAPIMiddleware = store => {
  const { dispatch, getState } = store;
  return next => action => {
    const {
      prefixType,
      callAPI,
      payload = {},
      shouldCallAPI = () => true,
    } = action;

    if (!prefixType) {
      // Normal action: pass it on
      return next(action);
    }

    if (!typeof prefixType === "string") {
      throw new Error("Expected a string of prefixType.");
    }

    if (typeof callAPI !== "function") {
      throw new Error("Expected callAPI to be a function.");
    }

    if (!shouldCallAPI(getState())) {
      return;
    }

    const requestType = `${prefixType}_REQUEST`;
    const successType = `${prefixType}_SUCCESS`;
    const failureType = `${prefixType}_FAILURE`;

    dispatch(dispatchAction(requestType));
    return callAPI().then(
      res => {
        const data = res.data;
        dispatch(dispatchAction(successType, data, payload));
      },
      error => {
        dispatch(dispatchAction(failureType, error));
        //dispatch(displayError(error));
      },
    );
  };
};

export default callAPIMiddleware;
