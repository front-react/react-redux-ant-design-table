import { ERROR_HANDLER } from "../types";
import { createReducer } from "./functions";

const initialState = { errorPortail: "" };

const errorPortailHandlers = {
  [ERROR_HANDLER](state, action) {
    return { ...state, errorPortail: action.payload.errorHandler };
  },
};

const appReducer = createReducer(initialState, {
  ...errorPortailHandlers,
});

export default appReducer;
