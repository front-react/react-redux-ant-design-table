import { combineReducers } from "redux";
import appReducer from "./appReducer";
import clientsReducer from "./../../modules/client/redux/reducers";
import modelesReducer from "./../../modules/modele/redux/reducers";
import sousModelesReducer from "./../../modules/sousModele/redux/reducers";
import produitsReducer from "./../../modules/produit/redux/reducers";
import prescripteursReducer from "./../../modules/prescripteur/redux/reducers";
import pdcsReducers from "./../../modules/pdc/redux/reducers";
import emplacementsReducer from "./../../modules/emplacement/redux/reducers";
import iocsReducers from "./../../modules/ioc/redux/reducers";
import surveyManagerdReducers from "./../../modules/survey/redux/reducers";
import enquetesReducer from "./../../modules/enquete/redux/reducers";
import userReducer from "./userReducer";
//import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  app: appReducer,
  clients: clientsReducer,
  modeles: modelesReducer,
  sousModeles: sousModelesReducer,
  produits: produitsReducer,
  prescripteurs: prescripteursReducer,
  pdcs: pdcsReducers,
  emplacements: emplacementsReducer,
  iocs: iocsReducers,
  survey: surveyManagerdReducers,
  enquetes: enquetesReducer,
  user: userReducer,
  //form: formReducer,
});

export default rootReducer;
