const createApihandlers = (
  prefixType,
  successReducer = (state, action) => {
    return { ...state };
  },
  failureReducer = (state, action) => {
    return { ...state };
  },
) => {
  return {
    [`${prefixType}_REQUEST`](state, action) {
      return { ...state };
    },
    [`${prefixType}_SUCCESS`](state, action) {
      return successReducer(state, action);
    },
    [`${prefixType}_FAILURE`](state, action) {
      return failureReducer(state, action);
    },
  };
};

export default createApihandlers;
