const createAddItemInArray = stateValue => {
  return (state, action) => {
    return {
      ...state,
      [stateValue]: [...state[stateValue], action.payload.data],
    };
  };
};

export default createAddItemInArray;
