import { ERROR_HANDLER } from "./../../types";

const errorPortailAction = errorHandler => {
  return dispatch => {
    dispatch({ type: ERROR_HANDLER, payload: { errorHandler } });
  };
};

export default errorPortailAction;
