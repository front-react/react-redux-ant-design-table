import styled from "styled-components";

export const Title = styled.p`
  font-size: 40px;
  color: black;
  padding-left: 20px;
  margin-top: 40px;
`;

export const SubTitle = styled.p`
  font-size: 20px;
  color: black;
  padding-left: 20px;
  margin-top: 40px;
`;

export const Content = styled.div`
  padding: 5px;
  margin-top: 40px;
`;

export const InfosMetier = styled.div`
  background-color: rgb(245, 245, 245);
  padding: 5px;
  margin-top: 10px;
`;

export const Label = styled.span`
  font-size: 12px;
  font-weight: ${props => props.weight};
`;

export const ContentDiv = styled.div`
  margin-top: 20px;
  margin-bottom: 40px;
`;

export const ContentMap = styled.div`margin-top: 10px;`;

export const Divider = styled.div`
  display: block;
  height: 1px;
  width: 100%;
  margin: 10px 0;
  background-color: rgb(200, 200, 200);
`;

export const PaperApp = styled.div`margin: 0px 45px 20px 45px;`;
