import { GET_ALL_KPI_QUESTIONS_LIST } from "./../../../types";
import { kpiApiUpsellProvider } from "./../../../../../../lib/api/providers";

const getAllKpiQuestions = () => {
  return {
    prefixType: GET_ALL_KPI_QUESTIONS_LIST,
    callAPI: () => kpiApiUpsellProvider.get(`/getAllKpiQuestions`),
  };
};

export default getAllKpiQuestions;
