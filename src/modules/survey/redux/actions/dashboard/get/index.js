import getKpiList from "./getKpiList";
import getQuestionsByKpi from "./getQuestionsByKpi";
import getAllKpiQuestions from "./getAllKpiQuestions";
import getSurveyList from "./getSurveyList";
import getSectorsList from "./getSectorsList";

export {
  getKpiList,
  getQuestionsByKpi,
  getAllKpiQuestions,
  getSurveyList,
  getSectorsList,
};
