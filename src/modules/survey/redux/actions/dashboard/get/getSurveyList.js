import { GET_SURVEY_LIST } from "./../../../types";
import { kpiApiUpsellProvider } from "./../../../../../../lib/api/providers";

const getSurveyList = () => {
  return {
    prefixType: GET_SURVEY_LIST,
    callAPI: () => kpiApiUpsellProvider.get(`/getSurveyList`),
  };
};

export default getSurveyList;
