import { createSelector } from "reselect";
import _ from "lodash";
import { filter, keyBy, isPlainObject } from "lodash/fp";

const getSurveyList = (state, ownProps) =>
  state.survey.surveyManager.surveyList;

const getSurveyId = (state, ownProps) =>
  ownProps.match ? ownProps.match.params.id : ownProps.idSurvey;

const surveySelector = createSelector(
  [getSurveyList, getSurveyId],
  (surveyList, surveyId) => {
    //console.log(surveyId);
    // if (dashboardList === []) {
    //   return [];
    // }

    // //let pdvSelected = pdvsList.find(item => item.pdvId === pdvId);
    let surveySelected = _.filter(surveyList, ["id", parseInt(surveyId)]);
    //return dashboardSelected;
    let survey = surveySelected[0];
    return survey;

    // return pdv;
  },
);

export default surveySelector;
