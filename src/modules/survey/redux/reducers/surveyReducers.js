import {
  GET_KPIS_LIST,
  GET_QUESTIONS_BY_KPI,
  GET_ALL_KPI_QUESTIONS_LIST,
} from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  dashboardList: [
    { id: 45, DashboardName: "Dashboard 45" },
    { id: 65, DashboardName: "Dashboard 65" },
    { id: 13, DashboardName: "Dashboard 13" },
    { id: 48, DashboardName: "Dashboard 48" },
    { id: 150, DashboardName: "Dashboard 150" },
  ],
  kpisList: [
    { idKPI: 1, KPICol: "kpi1" },
    { idKPI: 2, KPICol: "kpi2" },
    { idKPI: 3, KPICol: "kpi3" },
    { idKPI: 4, KPICol: "kpi4" },
  ],
  surveyList: [
    {
      id: 12,
      surveyName: "GSK 2016",
      enseigne: "SUPER U",
      begin: "01/04/2016",
      end: "01/06/2016",
      periode: "T2",
      status: 1,
    },
    {
      id: 33,
      surveyName: "OP  BEAUTE",
      enseigne: "CFR Market",
      begin: "02/03/2018",
      end: "02/05/2018",
      periode: "2018",
      status: 2,
    },
    {
      id: 45,
      surveyName: "OP  BEAUTE",
      enseigne: "INTERMARCHE",
      begin: "03/04/2017",
      end: "03/06/2017",
      periode: "S1",
      status: 3,
    },
  ],
  allKpiQuestions: [],
  questionsByKpi: [
    {
      id: 1,
      nom: "kpi1",
      titre: "question1",
      type: "kpi",
      typeReponse: "string",
      kpi: 1,
      question: 1,
    },
    {
      id: 4,
      nom: "kpi1",
      titre: "question4",
      type: "kpi",
      typeReponse: "float",
      kpi: 1,
      question: 4,
    },
    {
      id: 5,
      nom: "kpi1",
      titre: "question5",
      type: "kpi",
      typeReponse: "float",
      kpi: 1,
      question: 5,
    },
    {
      id: 5,
      nom: "kpi2",
      titre: "question5",
      type: "kpi",
      typeReponse: "float",
      kpi: 2,
      question: 5,
    },
    {
      id: 2,
      nom: "kpi3",
      titre: "question2",
      type: "kpi",
      typeReponse: "string",
      kpi: 3,
      question: 2,
    },
    {
      id: 3,
      nom: "kpi3",
      titre: "question3",
      type: "kpi",
      typeReponse: "int",
      kpi: 3,
      question: 3,
    },
    {
      id: 1,
      nom: "kpi4",
      titre: "question1",
      type: "kpi",
      typeReponse: "string",
      kpi: 4,
      question: 1,
    },
  ],
};

const getKpisHandlers = createApiHandlers(GET_KPIS_LIST, (state, action) => {
  return { ...state, kpisList: action.payload.data };
});
const getquestionsByKpiHandlers = createApiHandlers(
  GET_QUESTIONS_BY_KPI,
  (state, action) => {
    return { ...state, questionsByKpi: action.payload.data };
  },
);
const getAllKpiquestionsHandlers = createApiHandlers(
  GET_ALL_KPI_QUESTIONS_LIST,
  (state, action) => {
    return { ...state, allKpiQuestions: action.payload.data };
  },
);

const surveyReducers = createReducer(initialState, {
  ...getKpisHandlers,
  ...getquestionsByKpiHandlers,
  ...getAllKpiquestionsHandlers,
});

export default surveyReducers;
