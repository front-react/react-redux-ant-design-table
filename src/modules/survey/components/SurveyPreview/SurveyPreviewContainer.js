import { connect } from "react-redux";
import SurveyPreview from "./SurveyPreview";
import { surveySelector } from "./../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
  //console.log(ownProps);
  return {
    currentSurvey: surveySelector(state, ownProps),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    // getPreviewSurvey: () => {
    //   dispatch(getPreviewSurvey());
    // },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyPreview);
