import React, { Component } from "react";
import {
  Title,
  ContentDiv,
  SubTitle,
  PaperApp,
} from "./../../StyledComponents/StyledComponents";

class SurveyPreview extends Component {
  render() {
    const { currentSurvey } = this.props;
    return (
      <PaperApp>
        <Title>{`Previsulaisation du ${currentSurvey.surveyName}`}</Title>
      </PaperApp>
    );
  }
}

export default SurveyPreview;
