import { connect } from "react-redux";
import SurveyList from "./SurveyList";

const mapStateToProps = (state, ownProps) => {
  return {
    surveyList: state.survey.surveyManager.surveyList,
    match: ownProps.match,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyList);
