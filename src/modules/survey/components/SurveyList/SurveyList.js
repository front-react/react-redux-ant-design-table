import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import {
  Table,
  Divider,
  Icon,
  Tooltip,
  notification,
  Button,
  Badge,
} from "antd";
import { PaperApp, Title } from "./../../StyledComponents/StyledComponents";
import DeleteSurvey from "./DeleteSurvey";
import Surveyinfos from "./Surveyinfos";

class SurveyList extends Component {
  state = {
    current: 1,
    pageSize: 2,
  };
  render() {
    const { surveyList, match } = this.props;

    const columns = [
      {
        key: "surveyName",
        title: "Nom questionnaire",
        dataIndex: "surveyName",
      },
      { key: "enseigne", title: "Enseigne", dataIndex: "enseigne" },
      { key: "begin", title: "Début", dataIndex: "begin" },
      { key: "end", title: "Fin", dataIndex: "end" },
      { key: "periode", title: "Période", dataIndex: "periode" },
      {
        key: "status",
        title: "Status",
        dataIndex: "status",
        render: (record, text) => {
          const status = parseInt(record);
          return (
            <Fragment>
              {[status].indexOf(1) !== -1 &&
                <Fragment>
                  <Badge status="success" />
                  <span>Validé</span>
                </Fragment>}
              {[status].indexOf(2) !== -1 &&
                <Fragment>
                  <Badge status="warning" />
                  <span>Généré</span>
                </Fragment>}
              {[status].indexOf(3) !== -1 &&
                <Fragment>
                  <Badge status="error" />
                  <span>Brouillon</span>
                </Fragment>}
            </Fragment>
          );
        },
      },
      {
        key: "id",
        title: "Action",
        dataIndex: "id",
        render: (record, text) => {
          const status = parseInt(text.status);
          return (
            <Fragment>
              <Link to={`${match.url}/${record}/surveyGestion`}>
                <Tooltip placement="top" title={"Edit"}>
                  <Icon
                    type="edit"
                    style={{ fontSize: 24, color: "#000000" }}
                  />
                </Tooltip>
              </Link>
              <Divider type="vertical" />
              <DeleteSurvey idSurvey={record} />
              {[status].indexOf(2) !== -1 &&
                <Fragment>
                  <Divider type="vertical" />
                  <Link to={`${match.url}/${record}/surveyGestion/preview`}>
                    <Tooltip placement="top" title={"Previsualiser"}>
                      <Icon
                        type="shake"
                        style={{ fontSize: 24, color: "#000000" }}
                      />
                    </Tooltip>
                  </Link>
                  <Divider type="vertical" />
                  <Link to={`${match.url}/${record}/surveyGestion/preview`}>
                    <Tooltip placement="top" title={"Dupliquer"}>
                      <Icon
                        type="copy"
                        style={{ fontSize: 24, color: "#000000" }}
                      />
                    </Tooltip>
                  </Link>
                  <Divider type="vertical" />
                  <Link to={`${match.url}/${record}/surveyGestion/preview`}>
                    <Tooltip placement="top" title={"Archiver"}>
                      <Icon
                        type="database"
                        style={{ fontSize: 24, color: "#000000" }}
                      />
                    </Tooltip>
                  </Link>
                </Fragment>}
              {[status].indexOf(1) !== -1 &&
                <Fragment>
                  <Divider type="vertical" />
                  <Link to={`${match.url}/${record}/surveyGestion/preview`}>
                    <Tooltip placement="top" title={"Previsualiser"}>
                      <Icon
                        type="shake"
                        style={{ fontSize: 24, color: "#000000" }}
                      />
                    </Tooltip>
                  </Link>
                  <Divider type="vertical" />
                  <Link to={`${match.url}/${record}/surveyGestion/preview`}>
                    <Tooltip placement="top" title={"Dupliquer"}>
                      <Icon
                        type="copy"
                        style={{ fontSize: 24, color: "#000000" }}
                      />
                    </Tooltip>
                  </Link>
                  <Divider type="vertical" />
                  <Link to={`${match.url}/${record}/surveyGestion/preview`}>
                    <Tooltip placement="top" title={"Archiver"}>
                      <Icon
                        type="database"
                        style={{ fontSize: 24, color: "#000000" }}
                      />
                    </Tooltip>
                  </Link>
                </Fragment>}
            </Fragment>
          );
        },
      },
    ];

    return (
      <div>
        <Title>{`Liste des questionnaires`}</Title>
        <Table
          columns={columns}
          dataSource={surveyList}
          //size={2}
          pagination={{
            total: surveyList.length,
            current: this.state.current,
            pageSize: this.state.pageSize,
            onChange: (page, pageSize) => {
              this.setState({
                current: page,
                pageSize,
              });
            },
          }}
          rowKey={record => record.id}
          expandedRowRender={record => <Surveyinfos {...record} />}
        />
      </div>
    );
  }
}

export default SurveyList;
