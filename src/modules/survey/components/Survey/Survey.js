import React from "react";
import { Route, Switch } from "react-router-dom";
//import { IndexRedirect, IndexRoute } from "react-router";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import SurveyList from "./../SurveyList";
import SurveyGestion from "./../SurveyGestion";
import SurveyPreview from "./../SurveyPreview";

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

const Survey = ({ match, collapseBar }) => {
  const { url } = match;
  return (
    <div>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          trigger={null}
          collapsible
          collapsed={collapseBar}
          //onCollapse={this.onCollapse}
        >
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="vertical">
            <Menu.Item key="1" style={{ margin: 0 }}>
              <Icon type="pie-chart" />
              <span>Option 1</span>
            </Menu.Item>
            <Menu.Item key="2" style={{ margin: 0 }}>
              <Icon type="desktop" />
              <span>Option 2</span>
            </Menu.Item>
            <Menu.Item key="9" style={{ margin: 0 }}>
              <Icon type="file" />
              <span>File</span>
            </Menu.Item>
            <SubMenu
              key="sub1"
              title={
                <span>
                  <Icon type="mail" />
                  <span>Navigation One</span>
                </span>
              }
              style={{ margin: 0 }}
            >
              <Menu.Item key="1">Option 1</Menu.Item>
              <Menu.Item key="2">Option 2</Menu.Item>
              <Menu.Item key="3">Option 3</Menu.Item>
              <Menu.Item key="4">Option 4</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout>
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>User</Breadcrumb.Item>
              <Breadcrumb.Item>Bill</Breadcrumb.Item>
            </Breadcrumb>
            <Switch>
              <Route
                name="SurveyList"
                exact
                path={`${url}/`}
                component={props => <SurveyList {...props} />}
              />
              <Route
                name="SurveyGestion"
                exact
                path={`${url}/:id/surveyGestion`}
                component={props => <SurveyGestion {...props} />}
              />
              <Route
                name="SurveyPreview"
                exact
                path={`${url}/:id/surveyGestion/preview`}
                component={props => <SurveyPreview {...props} />}
              />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default Survey;
