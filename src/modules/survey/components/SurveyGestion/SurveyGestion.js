import React, { Component, Fragment } from "react";
import { PaperApp, Title } from "./../../StyledComponents/StyledComponents";
import { Steps, Row, Col, Button, message, Divider } from "antd";
//import KpiList from "./KpiList";
import KpiListSelect from "./KpiListSelect";
import PeriodSurveySelect from "./PeriodSurveySelect";
const Step = Steps.Step;

class SurveyGestion extends Component {
  state = {
    current: 0,
  };
  next = () => {
    const current = this.state.current + 1;
    this.setState({ current });
  };

  prev = () => {
    const current = this.state.current - 1;
    this.setState({ current });
  };

  continue = index => {
    this.setState({ current: index });
  };
  render() {
    const { currentSurvey, steps } = this.props;
    const { current } = this.state;
    const status = parseInt(currentSurvey.status);
    return (
      <PaperApp>
        <Title>{`Gestion du questionnaire ${currentSurvey.surveyName}`}</Title>
        <Row gutter={24}>
          <Col span={4}>
            <span>
              {"Résumer de la selection"}
            </span>
          </Col>
          <Col span={20}>
            <Fragment>
              {[status].indexOf(1) !== -1
                ? <Fragment>
                    <Steps current={current}>
                      {steps.map((item, index) =>
                        <Step
                          key={item.title}
                          title={item.title}
                          onClick={() => this.continue(index)}
                        />,
                      )}
                    </Steps>
                  </Fragment>
                : <Fragment>
                    <Steps current={current}>
                      {steps.map((item, index) =>
                        <Step key={item.title} title={item.title} />,
                      )}
                    </Steps>
                  </Fragment>}
            </Fragment>
            {current === 0 && <KpiListSelect idSurvey={currentSurvey.id} />}
            {current === 1 &&
              <PeriodSurveySelect idSurvey={currentSurvey.id} />}
            <Divider type="horizontal" />
            <Fragment>
              {current > 0 &&
                <Fragment>
                  <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                    Previous
                  </Button>
                  <Divider type="vertical" />
                </Fragment>}
              {current < steps.length - 1 &&
                <Fragment>
                  <Button type="primary" onClick={() => this.next()}>
                    Next
                  </Button>
                </Fragment>}
              {current === steps.length - 1 &&
                <Fragment>
                  <Button
                    type="primary"
                    onClick={() =>
                      message.success("Vous avez fini la creation ou modi")}
                  >
                    Done
                  </Button>
                </Fragment>}
            </Fragment>
          </Col>
        </Row>
      </PaperApp>
    );
  }
}

SurveyGestion.defaultProps = {
  steps: [
    {
      title: "KPI",
    },
    {
      title: "Période",
    },
    {
      title: "Secteur",
    },
    {
      title: "Circuit",
    },
    {
      title: "Enseigne",
    },
    {
      title: "Emplacement",
    },
    {
      title: "Critères",
    },
  ],
};

export default SurveyGestion;
