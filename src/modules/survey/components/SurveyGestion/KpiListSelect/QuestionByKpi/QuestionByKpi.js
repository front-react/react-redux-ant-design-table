import React, { Component, Fragment } from "react";
import { Table, Badge, Menu, Dropdown, Icon, Divider } from "antd";

const QuestionByKpi = ({ allKpiQuestions }) => {
  const columns = [
    { title: "Nom", dataIndex: "titre", key: "titre" },
    { title: "Type", dataIndex: "typeReponse", key: "typeReponse" },
    {
      title: "Action",
      dataIndex: "id",
      key: "id",
      render: () =>
        <span className="table-operation">
          <a href="javascript:;">Pause</a>
          <Divider type="vertical" />
          <a href="javascript:;">Stop</a>
        </span>,
    },
  ];

  return (
    <Table columns={columns} dataSource={allKpiQuestions} pagination={false} />
  );
};
export default QuestionByKpi;
