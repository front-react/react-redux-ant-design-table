import React, { Component, Fragment } from "react";
import { Table, Badge, Menu, Dropdown, Icon } from "antd";
import QuestionByKpi from "./QuestionByKpi";

class KpiListSelect extends Component {
  render() {
    const { kpisList } = this.props;

    const columns = [
      { title: "Name Kpi", dataIndex: "KPICol", key: "KPICol" },
      {
        title: "Action",
        dataIndex: "idKPI",
        key: "idKPI",
        render: (record, text) =>
          <a>
            Actions kpi {`${record}`}
          </a>,
      },
    ];

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        console.log(
          `selectedRowKeys: ${selectedRowKeys}`,
          "selectedRows: ",
          selectedRows,
        );
      },
      onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
      },
      onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
      },
    };

    return (
      <Fragment>
        <Table
          className="components-table-demo-nested"
          columns={columns}
          expandedRowRender={record => <QuestionByKpi idKpi={record.idKPI} />}
          rowSelection={rowSelection}
          dataSource={kpisList}
          pagination={false}
        />
      </Fragment>
    );
  }
}

export default KpiListSelect;
