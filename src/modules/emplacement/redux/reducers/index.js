import { combineReducers } from "redux";
import emplacementReducers from "./emplacementReducers";

const emplacementsReducers = combineReducers({
  emplacements: emplacementReducers,
});

export default emplacementsReducers;
