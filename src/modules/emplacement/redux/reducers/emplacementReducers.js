import { GET_EMPLACEMENTS_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  emplacementsList: [],
};

const getEmplacementstHandlers = createApiHandlers(
  GET_EMPLACEMENTS_LIST,
  (state, action) => {
    return { ...state, emplacementsList: action.payload.data };
  },
);

const emplacementReducers = createReducer(initialState, {
  ...getEmplacementstHandlers,
});

export default emplacementReducers;
