import { GET_CLIENTS_LIST } from "./../../types";
import { emplacementsApiUpsellProvider } from "./../../../../../lib/api/providers";

const getEmplacementsList = () => {
  return {
    prefixType: GET_CLIENTS_LIST,
    callAPI: () => emplacementsApiUpsellProvider.get(`/emplacements`),
  };
};

export default getEmplacementsList;
