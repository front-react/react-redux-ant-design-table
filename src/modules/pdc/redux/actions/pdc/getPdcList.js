import { GET_MODELES_LIST } from "./../../types";
import { pdcApiUpsellProvider } from "./../../../../../lib/api/providers";

const getPdcList = () => {
  return {
    prefixType: GET_MODELES_LIST,
    callAPI: () => pdcApiUpsellProvider.get(`/pdcs`),
  };
};

export default getPdcList;
