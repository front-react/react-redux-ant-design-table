import { GET_PDCS_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  pdcList: [],
};

const getPdcsHandlers = createApiHandlers(GET_PDCS_LIST, (state, action) => {
  return { ...state, pdcList: action.payload.data };
});

const pdcReducers = createReducer(initialState, {
  ...getPdcsHandlers,
});

export default pdcReducers;
