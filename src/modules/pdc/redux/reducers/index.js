import { combineReducers } from "redux";
import pdcReducers from "./pdcReducers";

const pdcsReducers = combineReducers({
  iocs: pdcReducers,
});

export default pdcsReducers;
