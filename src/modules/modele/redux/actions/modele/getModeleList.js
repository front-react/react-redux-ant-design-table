import { GET_MODELES_LIST } from "./../../types";
import { modelesApiProvider } from "./../../../../../lib/api/providers";

const getModeleList = () => {
  return {
    prefixType: GET_MODELES_LIST,
    callAPI: () => modelesApiProvider.get(`/modeles`),
  };
};

export default getModeleList;
