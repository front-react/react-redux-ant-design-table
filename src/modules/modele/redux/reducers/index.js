import { combineReducers } from "redux";
import modeleReducers from "./modeleReducers";

const modelesReducers = combineReducers({
  iocs: modeleReducers,
});

export default modelesReducers;
