import { GET_MODELES_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  modelesList: [],
};

const getModelestHandlers = createApiHandlers(
  GET_MODELES_LIST,
  (state, action) => {
    return { ...state, modelesList: action.payload.data };
  },
);

const modeleReducers = createReducer(initialState, {
  ...getModelestHandlers,
});

export default modeleReducers;
