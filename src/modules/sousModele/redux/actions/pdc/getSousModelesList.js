import { GET_SOUS_MODELE_LIST } from "./../../types";
import { sousModelesApiProvider } from "./../../../../../lib/api/providers";

const getSousModelesList = () => {
  return {
    prefixType: GET_SOUS_MODELE_LIST,
    callAPI: () => sousModelesApiProvider.get(`/sousModele`),
  };
};

export default getSousModelesList;
