import { GET_SOUS_MODELE_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  sousModeleList: [],
};

const getSousModelestHandlers = createApiHandlers(
  GET_SOUS_MODELE_LIST,
  (state, action) => {
    return { ...state, sousModeleList: action.payload.data };
  },
);

const sousModeleReducers = createReducer(initialState, {
  ...getSousModelestHandlers,
});

export default sousModeleReducers;
