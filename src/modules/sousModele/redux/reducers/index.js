import { combineReducers } from "redux";
import sousModeleReducers from "./sousModeleReducers";

const sousModelesReducers = combineReducers({
  iocs: sousModeleReducers,
});

export default sousModelesReducers;
