import { combineReducers } from "redux";
import enqueteReducers from "./enqueteReducers";

const enquetesReducers = combineReducers({
  enquetes: enqueteReducers,
});

export default enquetesReducers;
