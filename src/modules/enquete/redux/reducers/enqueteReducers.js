import { GET_ENQUETES_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  enquetesList: [],
};

const getEnquetestHandlers = createApiHandlers(
  GET_ENQUETES_LIST,
  (state, action) => {
    return { ...state, enquetesList: action.payload.data };
  },
);

const enqueteReducers = createReducer(initialState, {
  ...getEnquetestHandlers,
});

export default enqueteReducers;
