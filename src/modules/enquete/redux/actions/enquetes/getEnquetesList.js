import { GET_ENQUETES_LIST } from "./../../types";
import { enquetesApiUpsellProvider } from "./../../../../../lib/api/providers";

const getEnquetesList = () => {
  return {
    prefixType: GET_ENQUETES_LIST,
    callAPI: () => enquetesApiUpsellProvider.get(`/enquetes`),
  };
};

export default getEnquetesList;
