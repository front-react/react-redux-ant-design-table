import { GET_PRESCRIPTEURS_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  prescripteursList: [],
};

const getPrescripteursHandlers = createApiHandlers(
  GET_PRESCRIPTEURS_LIST,
  (state, action) => {
    return { ...state, prescripteursList: action.payload.data };
  },
);

const prescripteurReducers = createReducer(initialState, {
  ...getPrescripteursHandlers,
});

export default prescripteurReducers;
