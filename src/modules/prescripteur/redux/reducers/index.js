import { combineReducers } from "redux";
import prescripteurReducers from "./prescripteurReducers";

const prescripteursReducers = combineReducers({
  iocs: prescripteurReducers,
});

export default prescripteursReducers;
