import { GET_PRESCRIPTEURS_LIST } from "./../../types";
import { prescripteursApiProvider } from "./../../../../../lib/api/providers";

const getprescripteurList = () => {
  return {
    prefixType: GET_PRESCRIPTEURS_LIST,
    callAPI: () => prescripteursApiProvider.get(`/prescripteurs`),
  };
};

export default getprescripteurList;
