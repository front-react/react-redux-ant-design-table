import { GET_PRODUITS_LIST } from "./../../types";
import { produitsApiProvider } from "./../../../../../lib/api/providers";

const getProduitsList = () => {
  return {
    prefixType: GET_PRODUITS_LIST,
    callAPI: () => produitsApiProvider.get(`/produits`),
  };
};

export default getProduitsList;
