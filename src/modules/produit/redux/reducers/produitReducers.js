import { GET_PRODUITS_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  produitsList: [],
};

const getProduitstHandlers = createApiHandlers(
  GET_PRODUITS_LIST,
  (state, action) => {
    return { ...state, produitsList: action.payload.data };
  },
);

const produitReducers = createReducer(initialState, {
  ...getProduitstHandlers,
});

export default produitReducers;
