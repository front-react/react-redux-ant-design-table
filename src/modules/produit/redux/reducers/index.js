import { combineReducers } from "redux";
import produitReducers from "./produitReducers";

const produitsReducers = combineReducers({
  iocs: produitReducers,
});

export default produitsReducers;
