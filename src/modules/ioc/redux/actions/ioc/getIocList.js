import { GET_IOCS_LIST } from "./../../types";
import { iocApiUpsellProvider } from "./../../../../../lib/api/providers";

const getIocList = () => {
  return {
    prefixType: GET_IOCS_LIST,
    callAPI: () => iocApiUpsellProvider.get(`/iocs`),
  };
};

export default getIocList;
