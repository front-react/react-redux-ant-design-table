import { GET_IOCS_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  iocsList: [],
};

const getIocstHandlers = createApiHandlers(GET_IOCS_LIST, (state, action) => {
  return { ...state, iocsList: action.payload.data };
});

const iocReducers = createReducer(initialState, {
  ...getIocstHandlers,
});

export default iocReducers;
