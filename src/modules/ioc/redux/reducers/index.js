import { combineReducers } from "redux";
import iocReducers from "./iocReducers";

const iocsReducers = combineReducers({
  iocs: iocReducers,
});

export default iocsReducers;
