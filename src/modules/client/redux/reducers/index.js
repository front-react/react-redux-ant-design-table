import { combineReducers } from "redux";
import clientReducers from "./clientReducers";

const clientsReducer = combineReducers({
  clients: clientReducers,
});

export default clientsReducer;
