import { GET_CLIENTS_LIST } from "../types";

import {
  createReducer,
  createApiHandlers,
  createAddItemsInArrayWithFilter,
  createAddItemInArray,
  createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  clientsList: [],
};

const getClientsListHandlers = createApiHandlers(
  GET_CLIENTS_LIST,
  (state, action) => {
    return { ...state, clientsList: action.payload.data };
  },
);

const clientReducers = createReducer(initialState, {
  ...getClientsListHandlers,
});

export default clientReducers;
