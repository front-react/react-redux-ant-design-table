import { GET_CLIENTS_LIST } from "./../../types";
import { clientsApiProvider } from "./../../../../../lib/api/providers";

const getClientsList = () => {
  return {
    prefixType: GET_CLIENTS_LIST,
    callAPI: () => clientsApiProvider.get(`/clients`),
  };
};

export default getClientsList;
