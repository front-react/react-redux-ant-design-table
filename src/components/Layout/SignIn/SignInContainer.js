import { connect } from "react-redux";
import SignIn from "./SignIn";
import {
  setIdentityUser,
  ErrorStatusLogin,
} from "./../../../redux/actions/user";

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user.userCognito,
    errorUser: state.user.errorUser,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setIdentityUser: data => {
      dispatch(setIdentityUser(data));
    },
    ErrorStatusLogin: data => {
      dispatch(ErrorStatusLogin(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
