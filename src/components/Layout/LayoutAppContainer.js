import { connect } from "react-redux";
import LayoutApp from "./LayoutApp";

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LayoutApp);
