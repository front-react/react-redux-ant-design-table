import { connect } from "react-redux";
import ForgotPasswordAws from "./ForgotPasswordAws";
import { ErrorStatusLogin } from "./../../../redux/actions/user";

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    ErrorStatusLogin: data => {
      dispatch(ErrorStatusLogin(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(ForgotPasswordAws);
