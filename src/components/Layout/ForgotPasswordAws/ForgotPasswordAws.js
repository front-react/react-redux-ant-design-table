import React, { Component } from "react";
import UserNameView from "./UserNameView";
import UserNameSubmit from "./UserNameSubmit";
import { PaperLogin } from "./../loginStyle/loginStyle";
import { ErrorStatusLogin } from "./../../../redux/actions/user";

class ForgotPasswordAws extends Component {
  state = {
    delivery: null,
    username: null,
  };
  handleSignIn = () => {
    this.props.onStateChange("signIn");
    this.props.ErrorStatusLogin(null);
  };
  handleForgotPassword = (delivery, username) => {
    this.setState({ delivery, username });
  };
  handleForgotPasswordSubmit = deliveryNull => {
    this.setState({ delivery: deliveryNull });
    this.props.onStateChange("signIn");
  };

  render() {
    if (this.props.authState !== "forgotPassword") {
      return null;
    }

    return (
      <React.Fragment>
        {this.state.delivery
          ? <UserNameSubmit
              handleForgotPasswordSubmit={this.handleForgotPasswordSubmit}
              username={this.state.username}
              handleSignIn={this.handleSignIn}
            />
          : <UserNameView
              handleForgotPassword={this.handleForgotPassword}
              handleSignIn={this.handleSignIn}
            />}
      </React.Fragment>
    );
  }
}
export default ForgotPasswordAws;
