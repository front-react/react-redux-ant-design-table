import React, { Component } from "react";
import { Form as FinalForm } from "react-final-form";
import Form from "./Form";
import { Auth, I18n } from "aws-amplify";
import { Alert } from "antd";
import {
  PaperLogin,
  MarginLogin,
  BorderLogo,
  FormLogin,
  ButtonLogin,
  AlertBox,
} from "./../../loginStyle/loginStyle";
import { I18n_FR } from "./../../../I18nFr/I18nFr";
import "./../../../../lib/amplifyAws/configAmplify";
I18n.setLanguage("fr");
I18n.putVocabularies(I18n_FR);

class UserNameSubmit extends Component {
  onSubmit = values => {
    console.log(values);
    Auth.forgotPasswordSubmit(this.props.username, values.code, values.password)
      .then(data => {
        this.props.handleForgotPasswordSubmit(null);
        this.props.NotificationStatusLogin(null);
      })
      .catch(err => this.props.ErrorStatusLogin(err));
  };
  onClose = () => {
    this.props.ErrorStatusLogin(null);
  };
  onCloseNotif = () => {
    this.props.NotificationStatusLogin(null);
  };
  render() {
    const { initialValues, errorUser, handleSignIn, notifUser } = this.props;
    return (
      <PaperLogin>
        <MarginLogin>
          <BorderLogo />
          {notifUser &&
            <AlertBox>
              <Alert
                description={notifUser}
                type="warning"
                closable
                onClose={this.onCloseNotif}
              />
            </AlertBox>}
          <FormLogin>
            <FinalForm
              onSubmit={this.onSubmit}
              initialValues={initialValues}
              render={props => <Form {...props} />}
            />
          </FormLogin>
          <ButtonLogin onClick={handleSignIn}>
            Revenir à la page de connexion
          </ButtonLogin>
          {errorUser &&
            <AlertBox>
              <Alert
                message={I18n.get(errorUser.code)}
                description={
                  errorUser.message
                    ? I18n.get(errorUser.message)
                    : I18n.get(errorUser)
                }
                type="error"
                closable
                onClose={this.onClose}
              />
            </AlertBox>}
        </MarginLogin>
      </PaperLogin>
    );
  }
}

export default UserNameSubmit;
