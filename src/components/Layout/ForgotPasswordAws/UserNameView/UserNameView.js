import React, { Component } from "react";
import { Form as FinalForm } from "react-final-form";
import Form from "./Form";
import { Auth, I18n } from "aws-amplify";
import { Alert } from "antd";
import {
  PaperLogin,
  MarginLogin,
  BorderLogo,
  FormLogin,
  ButtonLogin,
  AlertBox,
} from "./../../loginStyle/loginStyle";
import { I18n_FR } from "./../../../I18nFr/I18nFr";
import "./../../../../lib/amplifyAws/configAmplify";
I18n.setLanguage("fr");
I18n.putVocabularies(I18n_FR);

class UserNameView extends Component {
  onSubmit = values => {
    Auth.forgotPassword(values.username)
      .then(data => {
        this.props.handleForgotPassword(
          data.CodeDeliveryDetails,
          values.username,
        );
        this.props.NotificationStatusLogin(
          "Un code vient de vous être envoyé sur votre mail UpSell !",
        );
      })
      .catch(err => this.props.ErrorStatusLogin(err));
  };

  onClose = () => {
    this.props.ErrorStatusLogin(null);
  };
  render() {
    const { initialValues, errorUser, handleSignIn } = this.props;
    return (
      <PaperLogin>
        <MarginLogin>
          <BorderLogo />
          <FormLogin>
            <FinalForm
              onSubmit={this.onSubmit}
              initialValues={initialValues}
              render={props => <Form {...props} />}
            />
          </FormLogin>
          <ButtonLogin onClick={handleSignIn}>
            Revenir à la page de connexion
          </ButtonLogin>
          {errorUser &&
            <AlertBox>
              <Alert
                message={I18n.get(errorUser.code)}
                description={
                  errorUser.message
                    ? I18n.get(errorUser.message)
                    : I18n.get(errorUser)
                }
                type="error"
                closable
                onClose={this.onClose}
              />
            </AlertBox>}
        </MarginLogin>
      </PaperLogin>
    );
  }
}

export default UserNameView;
