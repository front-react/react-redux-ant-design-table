import { connect } from "react-redux";
import UserNameView from "./UserNameView";
import { ErrorStatusLogin } from "./../../../../redux/actions/user";
import { NotificationStatusLogin } from "./../../../../redux/actions/user";

const mapStateToProps = (state, ownProps) => {
  return {
    errorUser: state.user.errorUser,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    ErrorStatusLogin: data => {
      dispatch(ErrorStatusLogin(data));
    },
    NotificationStatusLogin: data => {
      dispatch(NotificationStatusLogin(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserNameView);
