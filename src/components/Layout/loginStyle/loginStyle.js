import styled from "styled-components";
import img from "./bg_login/logo.png";
import { Input, Alert } from "antd";
import { SubmitButtonIconLabel } from "up-componup";
import SideNav, {
  Toggle,
  Nav,
  NavItem,
  NavIcon,
  NavText,
} from "@trendmicro/react-sidenav";

export const PaperLogin = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-image: linear-gradient(-45deg, #d7816a, #bd4f6c);
  background-size: cover;
  overflow: hidden;
`;

export const MarginLogin = styled.div`
  margin: 50px auto 0px auto;
  margin-bottom: 0;
  width: 450px;
  background-color: #ffffff;
  border-top: 8px solid #ed8b00;
  margin: 45vh auto 0px;
  transform: translateY(-50%);
  /* box-shadow: -1px 9px 21px #a9a9a9; */
`;

export const PositionLogin = styled.div`padding: 5% 0%;`;

export const TitleLogin = styled.p`
  color: #727272;
  font-size: 14px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 20px;
  font-weight: 700;
  padding: 0px;
`;

export const BorderLogo = styled.div`
  width: 140px;
  height: 140px;
  text-align: center;
  margin-top: 40px;
  margin-bottom: 30px;
  margin: 40px auto 30px auto;
  line-height: 180px;
  border-radius: 50%;
  background-image: url(${img});
  background-size: cover;
`;

export const ButtonLogin = styled.a`
  display: block;
  text-align: right;
  font-size: 16px;
  color: #727272;
  padding-bottom: 30px;
  padding-right: 40px;
`;

export const ButtonMdp = styled.a`
  display: block;
  text-align: right;
  font-size: 12px;
  color: #727272;
  padding-bottom: 45px;
  padding-right: 40px;
`;

export const FormLogin = styled.div`
  margin-left: 40px;
  margin-right: 40px;
`;

export const FormInput = styled.div`margin-top: 20px;`;
export const AlertBox = styled.div`
  margin: 0px 40px 0px 40px;
  padding-bottom: 40px;
`;

export const InputAnt = styled(Input)`
  font-size:20px;
  color: #727272 !important;
  .ant-input{
    height: 46px;
    width: 366px;
    color: #727272 !important;
    text-indent: 10px;
    border: 1px solid #A9A9A9;
    font-size: 14px;
  }
  input:-webkit-autofill {
    -webkit-text-fill-color: #A9A9A9 !important;
  }
  input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 250px white inset !important;
  }
`;

export const SubmitButtonIconLabelAnt = styled(SubmitButtonIconLabel)`
    width: 100% !important;
    height: 60px;
    background-color: #CC5803 !important;
    border-radius: 5px !important;
    color: #FFFFFF !important;
    margin-top: 20px !important;
    margin-bottom: 5px !important;
    font-size: 16px !important;
    .sc-kAzzGY{
      font-size: 16px;
    }
`;

export const AlertAnt = styled(Alert)`
    width: 400px !important;
`;
