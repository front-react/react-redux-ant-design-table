import { connect } from "react-redux";
import ConfirmSignIn from "./ConfirmSignIn";
import { ErrorStatusLogin } from "./../../../redux/actions/user";

const mapStateToProps = (state, ownProps) => {
  return {
    errorUser: state.user.errorUser,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    ErrorStatusLogin: data => {
      dispatch(ErrorStatusLogin(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmSignIn);
