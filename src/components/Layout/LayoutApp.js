import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import { withStyles } from "material-ui/styles";
import { orange } from "material-ui/colors";
import {
  ConfirmSignUp,
  SignUp,
  VerifyContact,
  withAuthenticator,
} from "aws-amplify-react";
import SignIn from "./SignIn";
import LogoutAws from "./LogoutAws";
import ForgotPasswordAws from "./ForgotPasswordAws";
import RequireNewPassword from "./RequireNewPassword";
import ConfirmSignIn from "./ConfirmSignIn";
import Survey from "./../../modules/survey/components/Survey";
//import { Icon } from "react-icons-kit";
//import { newspaperO } from "react-icons-kit/fa/newspaperO";
import { Menu, Icon } from "antd";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";
import "./../../lib/amplifyAws/configAmplify";

const styles = theme => ({
  content: {
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 2,
  },
  logo: {
    height: "32px",
    margin: "16px",
  },
  sider: {
    backgroundColor: orange[500],
  },
  menu: {
    backgroundColor: orange[500],
  },
});

class LayoutApp extends Component {
  state = {
    collapsed: false,
    current: "mail",
  };
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  handleClick = e => {
    this.setState({
      current: e.key,
    });
  };
  render() {
    const {} = this.props;

    return (
      <div>
        <Menu
          onClick={this.handleClick}
          selectedKeys={[this.state.current]}
          mode="horizontal"
        >
          <Menu.Item key="menu" style={{ height: "100%" }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
              onClick={this.toggle}
              style={{ height: 36 }}
            />
          </Menu.Item>
          <Menu.Item key="survey">
            <Link to="/survey">
              <Icon type="layout" style={{ height: 36 }} />
              <span style={{ color: "black", fontSize: "12px" }}>Survey</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="logout">
            <LogoutAws onStateChange={this.props.onStateChange} />
          </Menu.Item>
        </Menu>
        {/* <nav style={{ backgroundColor: "#BD4F6C", height: "65px" }}>
          <ul style={{ padding: 0, margin: 0, textAlign: "center" }}>
            <li style={{ display: "inline-block" }}>
              <Link to="/survey">
                <Icon
                  icon={newspaperO}
                  size={28}
                  style={{
                    color: "white",
                    width: 24,
                    height: 24,
                    margin: 8,
                  }}
                />
                <span style={{ color: "white", fontSize: "12px" }}>Survey</span>
              </Link>
            </li>
            <li style={{ display: "inline-block" }}>
              <LogoutAws onStateChange={this.props.onStateChange} />
            </li>
          </ul>
        </nav> */}
        <Route
          path="/survey"
          component={props =>
            <Survey {...props} collapseBar={this.state.collapsed} />}
        />
      </div>
    );
  }
}

export default withAuthenticator(withStyles(styles)(LayoutApp), false, [
  <SignIn />,
  <ConfirmSignIn />,
  <RequireNewPassword />,
  <VerifyContact />,
  <SignUp />,
  <ConfirmSignUp />,
  <ForgotPasswordAws />,
]);
